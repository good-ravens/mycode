#!/usr/bin/python

# Copyright: (c) 2020, Chris Carducci <ccarducci@bloomberg.net>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import requests
import json
DOCUMENTATION = r'''
---
module: getpokemon

short_description: Get info about a pokemon and optionally save as a file

version_added: "1.0.0"

description: uses pokeapi.co to make HTTP requests on a pokemon name. If a user provides a file path, the module saves the returned data to the file (pokedex)

options:
    pokemon:
        description: This is the name of the pokemon to search for within the pokeapi.co
        required: true
        type: str
    api_endpoint:
        description:
            - If the user wants to limit theresults, they can pass an API endpoint to do so
            - Bydefault, the api_endpoint "pokemon" will be used (this returns everything)
        required: false
        type: str
        default: pokemon
        options: (api endpoint -> name to use in playbook)
            - Abilities - ability 
            - Charactiristics - characteristic
            - Egg Groups - egg-group
            - Genders -gender
            - Growth Rates - growth-rate
            - Natures - nature
            - Pokeathlon Stats - pokeathalon-stat
            - Location - pokemon-location-area
            - Pokemon - pokemon
            - Pokemon Colors - pokemon-color
            - Pokemon Forms - pokemon-form
            - Pokemon Habitats - pokemon-habitat
            - Pokemon Shapes - pokemon-shapes
            - Pokemon Species - pokemon-species
            - Stats -stat
            - Types - type 

    poke_dex:

author:
    - Chris Carducci (@patchmonkey)
'''

EXAMPLES = r'''
# Pass in a message
- name: Lookup Pikachu
  ccarducci.pokemon_pokeapi.getpokemon:
    Pokemon: Pikachu
  register: results

- name: Display results
  debug:
    var: results

# save the results (json) to a file
- name: Create a pokedex that contains the results
  ccarducci.pokemon_pokeapi.getpokemon:
    pokemon: squirtle
    poke_dex: ~/my_pokedex.json

# only lookup info from a certain api endpoint
- name: Return just the forms associated with ditto
  ccarducci.pokemon_pokeapi.getpokemon:
    pokemon: ditto
    api_endpoint: pokemon-form
  register: results 

- name: Display results
  debug:
    var: results

'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
pokemon: 
    description: the pokemon name as it was looked up (always lower case)
    type: str
    returned: always
    sample: 'pikachu'
pokeapi_results:
    description: this is the data that was obtained from pokeapi.co
    type: dict
    returned: always
    sample: see https://pokeapi.co/api/v2/pokemon/ditto
status_code: 
    description: this is the http status code that was returned form the operation
    type: str
    returned: always
    sample: https://pokeapi.co/api/v2/pokemon/ditto
    


'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        pokemon=dict(type='str', required=True),
        api_endpoint=dict(type='str', required=False, default='pokemon'),
        poke_dex=dict(type='str', required=False)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        pokemon='', 
        pokeapi_results={},
        status_code='',
        url='',
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications


    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    pokeapi_endpoints = ["ability", "characteristic", "egg-group", "gender", "growth-rate", "nature", "pokeathlon-stat", "pokemon", "pokemon-location-area", "pokemon-color", "pokemon-form", "pokemon-habitat", "pokemon-shape", "pokemon-species", "stat", "type"]
    api_endpoint = module.params['api_endpoint'].lower()
    if api_endpoint not in pokeapi_endpoints:
        module.fail_json(msg=f'Uh oh! it looks like you requested an API endpoint that is not available. Legal values are: {pokeapi_enpoints}')

    pokemon = module.params["pokemon"].lower()
    result["pokemon"] = pokemon
    
    
    url = f"https://pokeapi.co/api/v2/{api_endpoint}/{pokemon}"
    if api_endpoint == "pokemon-location-area":
        url = url+ "/       encounters"


    result['url'] = url



    if module.check_mode:
        module.exit_json(**result)


    pokeapi_results = requests.get(url)
    result['status_code'] = pokeapi_results.status_code

    if pokeapi_results.status_code != 200:
        module.faile_json(msg=f"bad http status code{pokeapi_results.status_code}")
    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results

    result['pokeapi_results'] = pokeapi_results.json()

    pokedex = module.params.get('poke_dex')
    if pokedex:
        with open(pokedex, 'a') as pdex:
            pdex.write(json.dumps(result["pokeapi_results"]))

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()