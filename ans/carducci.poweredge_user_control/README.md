Role Name
=========

This role is a class project converting a playbook into role form. It utilizes omsdk to communicate with the iDrac controller of a DellEMC poweredge server, and it creates a new user. B

Requirements
------------

In order to use this solution first run, "python3 -m pip install omsdk". This also requires the dellemc.openmanage.idrac_user collection, which is usually present in the base ansible installation. 

Role Variables
--------------

  - 
    # the IP address of the idrac to which you want to connect
    idrac_ip: 192.168.1.163  
  -  
    # The idrac administrative user
    idrac_user: root               
  - 
    # The iDrac admin users's Password    
    idrac_password: r0gerwilc0         
  - 
    # The state of the user you want to create. "Present" means you should dcreate if it doesn't exist. 
    state: present
  - 
    # The username you want to create
    new_user_name: larry
  - 
    # The new user's password
    new_user_password: 0rangey0range   


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: carducci.poweredge_user_control, new_user_name: larry, new_user_password: abc123 }

License
-------

MIT

Author Information
------------------

Chris Carducci. A dude who does things. 
